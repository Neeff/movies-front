import { Form, Input, InputNumber, DatePicker, Select, SubmitButton } from 'formik-antd'
import { Formik } from 'formik';

const CreateCategoryForm = ({categoryToUpdate, updateCategory, createCategory }) => {
  const caseUseForm = () => {
    return Object.keys(categoryToUpdate).length !==0 ? 'Update' : 'Create'
  };
  const values = () => {
    return Object.keys(categoryToUpdate).length !== 0 ?
       categoryToUpdate :
       {
        name: ''
       }
  };

  return (
    <>
      <Formik
      initialValues={values()}
      onSubmit={(values) => {
        if (caseUseForm() === 'Update') {
          updateCategory(values);
        }
        else {
          createCategory(values);
        }
      }}
      enableReinitialize
      render={() => (
        <Form>
           <Form.Item
              label="Name"
              name="name"
              rules={[
                {
                  required: true,
                  message: 'Please input category name!',
                },
              ]}
            >
            <Input name='name' placeholder='Name' />
          </Form.Item>
            <SubmitButton disabled={false}>{caseUseForm()}</SubmitButton>
        </Form>
      )}
    />
  </>
  );
};

export default CreateCategoryForm;