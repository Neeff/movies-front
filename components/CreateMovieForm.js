import { Form, Input, InputNumber, DatePicker, Select, SubmitButton } from 'formik-antd'
import { Formik } from 'formik';

const CreateMovieForm = ({ categories, movieToUpdate, updateMovie, createMovie }) => {
  const caseUseForm = () => {
    return Object.keys(movieToUpdate).length !==0 ? 'Update' : 'Create'
  };
  const values = () => {
    return Object.keys(movieToUpdate).length !== 0 ?
       movieToUpdate :
       {
        name: '',
        stars: 0,
        released: '',
        cover: '',
        category_id: ''
       }
  };

  const optionsSelect = () => {
   return categories.map(category => ({ value: category.id, label: category.name }))
  }
  return (
    <>
      <Formik
      initialValues={values()}
      onSubmit={(values) => {
        if (caseUseForm() === 'Update') {
          updateMovie(values);
        }
        else {
          const valuesMovie = {...values, released: values.released.split('T')[0] }
          createMovie(valuesMovie);
        }
      }}
      enableReinitialize
      render={() => (
        <Form>
           <Form.Item
              label="Name"
              name="name"
              rules={[
                {
                  required: true,
                  message: 'Please input movie name!',
                },
              ]}
            >
            <Input name='name' placeholder='Name' />
          </Form.Item>
          <Form.Item
              label="Cover"
              name="cover"
              rules={[
                {
                  required: true,
                  message: 'Please input cover',
                },
              ]}
            >
            <Input name='cover' placeholder='Cover' />
          </Form.Item>
          <Form.Item
              label="Stars"
              name="stars"
              rules={[
                {
                  required: true,
                  message: 'Please input stars',
                },
              ]}
            >

          <InputNumber name='stars' min={0} />
            </Form.Item>
            <Form.Item
              label="Released"
              name="released"
              rules={[
                {
                  required: true,
                  message: 'Please input released',
                },
              ]}
            >
            <DatePicker name="released" showTime={false}></DatePicker>

            </Form.Item>
            <Form.Item
              label=""
              name="cover"
              rules={[
                {
                  required: true,
                  message: 'Please input cover',
                },
              ]}
            ></Form.Item>
            <Form.Item
              label="Category"
              name="category_id"
              rules={[
                {
                  required: true,
                  message: 'Please input category',
                },
              ]}
            >
            <Select options={optionsSelect()} defaultValue={movieToUpdate.category_id} name='category_id'/>

            </Form.Item>
            <SubmitButton disabled={false}>{caseUseForm()}</SubmitButton>
        </Form>
      )}
    />
  </>
  );
};

export default CreateMovieForm;