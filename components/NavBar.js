import React, { useState } from 'react';
import Link from 'next/link';
//import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import { Menu } from 'antd';

const items = [
  {
    label: (<Link href={'/movies/'}>Movies</Link>),
    key: 'movies'
  },
  {
    label: (<Link href={'/users/'}>Users</Link>),
    key: 'users'
  },
  {
    label: (<Link href={'/categories/'}>Categories</Link>),
    key: 'categories'
  }
];
const NavBar = () => {
  const [current, setCurrent] = useState('movies');
  const onClick = (e) => {
    setCurrent(e.key);
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
};

export default NavBar;