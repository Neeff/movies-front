import Link from 'next/link'
import { useEffect, useState, useMemo } from "react";
import { Table, Row, Col, Space, Button, notification , Modal} from "antd";
import axios from "axios";
import NavBar from '../../components/NavBar';
import CreateCategoryForm from '../../components/CreateCategoryForm';

const Categories = () => {
  let [categories, setCategories] = useState([]);
  let [error, setError] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [categoryToUpate, setCategoryToUpdate] = useState({});
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const [api, contextHolder] = notification.useNotification();
  const contextValue = useMemo(
    () => ({
      name: 'Ant Design',
    }),
    [],
  );

  const fetchCategories = async () => {
    let { data, status} = await axios.get('http://127.0.0.1:4000/api/categories/');
    setCategories(data);
  };

  const asyncCreateCategory = async (category) => {
    const { data, status } = await axios.post('http://127.0.0.1:4000/api/categories/new', category);
    const { message } = data;
    if (message === null || message === undefined) {
      setCategories([...categories, data]);
      openNotification('Category Created', 'Category created successfully 🎉');
      setIsModalOpen(false);
    }
    else {
      errorNotification(data.message, 'Ups, a error ocurred 😖');
      setIsModalOpen(false);
    }

  };
  const asyncUpdateCategory = async (category) => {
    const { data, status } = await axios.post(`http://127.0.0.1:4000/api/categories/update/${category.id}`, category);
    const { message } = data;
    if (message === null || message === undefined) {
      let newCategories = categories.map(item => item.id === data.id ? data : item);
      setCategories(newCategories);
      openNotification('Category Updated', 'Category updated successfully 🎉');
      setIsModalOpen(false);
      setCategoryToUpdate({});
    }
    else {
      errorNotification(data.message, 'Ups, a error ocurred 😖');
      setIsModalOpen(false);
      setCategoryToUpdate({});
    }

  };
  const createCategory = (category) =>  asyncCreateCategory(category);

  const updateCategory = (category) => asyncUpdateCategory(category);

  const openNotification = (message, placement) => {
    api.success({
      message: `Notification ${placement}`,
      description: message,
      placement,
    });
  };

  const errorNotification = (message, placement) => {
    api.error({
      message: `Notification ${placement}`,
      description: message,
      placement,
    });
  };

  const deleteCategory = async (id) => {
    // delete movie
    let { data, status} = await axios.delete(`http://127.0.0.1:4000/api/categories/delete/${id}`);
    if (status === 200) {
      let newCategories = categories.filter(category => category.id != id);
      openNotification(data['message'], 'Delete Resource');
      setCategories(newCategories);
    }
    else {
      errorNotification(data.message, 'Ups, a error ocurred')
    }
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  return(
    <>
    {contextHolder}
    <NavBar/>
      <Row justify="center">
        <Col span={12}>
        <Button onClick={() => {
          setIsModalOpen(true);
          setTitleModal('Create Category');
          setCategoryToUpdate({});
        }}>Create Category</Button>
        <Modal title={titleModal} open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer={[null, null]}>
          <CreateCategoryForm  categoryToUpdate={categoryToUpate} updateCategory={updateCategory} createCategory={createCategory}/>
        </Modal>
          { categories ?
            <Table columns={[
              { title: 'Name', dataIndex: 'name', key: 'name'},
              { title: 'Actions', key: 'actions', render: (_, record) => (
                <Space>
                  <Button onClick={() => {
                    setIsModalOpen(true);
                    setTitleModal('Update Category')
                    setCategoryToUpdate(record)
                  }}>Update</Button>
                  <Button onClick={() => deleteCategory(record.id)}>Delete</Button>
                </Space>
              )}
            ]} dataSource={categories}
            pagination={false} /> : null
        }
        </Col>
      </Row>
    </>
  )
};

export default Categories;