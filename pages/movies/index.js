import Link from 'next/link'
import { useEffect, useState, useMemo } from "react";
import { Table, Row, Col, Space, Button, notification, Modal } from "antd";
import axios from "axios";
import NavBar from '../../components/NavBar';
import CreateMovieForm from '../../components/CreateMovieForm';

const Movies = () => {
  const [movies, setMovies] = useState([]);
  const [categories, setCategories] = useState([]);
  const [error, setError] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [titleModal, setTitleModal] = useState('');
  const [movieToUpate, setMovieToUpdate] = useState({});
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const [api, contextHolder] = notification.useNotification();
  const contextValue = useMemo(
    () => ({
      name: 'Ant Design',
    }),
    [],
  );

  const fetchMovies = async () => {
    let { data, status} = await axios.get('http://127.0.0.1:4000/api/movies/');
    setMovies(data);
  };

  const fetchCategories = async() => {
    let { data, status } = await axios.get('http://127.0.0.1:4000/api/categories/');
    setCategories(data);
  };

  const asyncUpdateMovie = async movie => {
    const { data, status } = await axios.post(`http://127.0.0.1:4000/api/movies/update/${movie.id}`, movie);
    const { message } = data;
    if (message === null || message === undefined) {
      let newMovies = movies.map(item => item.id === data.id ? data : item);
      setMovies(newMovies);
      openNotification('Movie Updated', 'Movie updated successfully 🎉');
      setIsModalOpen(false);
      setMovieToUpdate({});
    }
    else {
      errorNotification(data.message, 'Ups, a error ocurred 😖');
      setIsModalOpen(false);
      setMovieToUpdate({});
    }
  }

  const asyncCreateMovie = async movie => {
    const { data, status } = await axios.post('http://127.0.0.1:4000/api/movies/new', movie);
    const { message } = data;
    if (message === null || message === undefined) {
      setMovies([...movies, data]);
      openNotification('Movie Created', 'Movie created successfully 🎉');
      setIsModalOpen(false);
    }
    else {
      errorNotification(data.message, 'Ups, a error ocurred 😖');
      setIsModalOpen(false);
    }
  };

  const createMovie = (movie) => {
    asyncCreateMovie(movie);
  };

  const updateMovie = (movie) => {
    asyncUpdateMovie(movie);
  };
  const openNotification = (message, placement) => {
    api.success({
      message: `Notification ${placement}`,
      description: message,
      placement,
    });
  };

  const errorNotification = (message, placement) => {
    api.error({
      message: `Notification ${placement}`,
      description: message,
      placement,
    });
  };

  const deleteMovie = async (id) => {
    // delete movie
    let { data, status} = await axios.delete(`http://127.0.0.1:4000/api/movies/delete/${id}`);
    if (status === 200) {
      let newMovies = movies.filter(movie => movie.id != id);
      openNotification(data['message'], 'Delete Resource');
      setMovies(newMovies);
    }
  };

  useEffect(() => {
    fetchMovies();
    fetchCategories();
  }, []);

  return(
    <>
    {contextHolder}
    <NavBar/>
      <Row justify="center">
        <Col span={12}>
        <Button onClick={() => {
          setIsModalOpen(true);
          setTitleModal('Create Movie');
          setMovieToUpdate({});
        }}>Create Movie</Button>
        <Modal title={titleModal} open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer={[null, null]}>
          <CreateMovieForm categories={categories} movieToUpdate={movieToUpate} updateMovie={updateMovie} createMovie={createMovie}/>
        </Modal>
          { movies ?
            <Table columns={[
              { title: 'Name', dataIndex: 'name', key: 'name'},
              { title: 'Released', dataIndex: 'released', key: 'released'},
              { title: 'Stars', dataIndex: 'stars', key: 'stars'},
              { title: 'Actions', key: 'actions', render: (_, record) => (
                <Space>
                  <Link href={`/movies/${record.id}`}>
                    <Button>View</Button>
                  </Link>
                  <Button onClick={() =>  { setIsModalOpen(true);
                                            setTitleModal('Update Movie')
                                            setMovieToUpdate(record)} }>
                                            Update</Button>
                  <Button onClick={() => deleteMovie(record.id)}>Delete</Button>
                </Space>
              )}
            ]} dataSource={movies}
            pagination={false} /> : null
        }
        </Col>
      </Row>
    </>
  )
};

export default Movies;