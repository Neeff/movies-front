import { useState, useEffect } from "react";
import { useRouter } from 'next/router';
import axios from "axios";
import { Card, Col, Row, Empty } from 'antd';

const { Meta } = Card;

const Movie = () => {
  const [movie, setMovie] = useState({});
  const [message, setMessage] = useState('');
  const router = useRouter();

  const getMovie = async (id) => {
    let { data } = await axios.get(`http://127.0.0.1:4000/api/movies/update/${id}`);
    let { message } = data;
    if (message === 'record not found') {
      setMessage(message);
    }
    else {
      setMovie(data);
    }
  };

  useEffect(()=> {
    //router.isReady
    const { id } = router.query;
    getMovie(id);
  }, [router.isReady, router.query]);

  return (
    <>
       <Row justify="center">
          <Col span={4}>
            {
              message && Object.keys(movie).length === 0
                ? <Empty/>
                : <Card
                hoverable
                style={{ width: 500 }}
                cover={<img alt="cover" src={movie.cover} />}
              >
                <Meta title={movie.name} description={movie.released} />
                <Meta title='Stars' description={'⭐️'.repeat(movie.stars)} />
              </Card>
            }
          </Col>
       </Row>
    </>
  )
};

export default Movie;