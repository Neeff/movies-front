import Link from 'next/link'
import { useEffect, useState, useMemo } from "react";
import { Table, Row, Col, Space, Button, notification} from "antd";
import axios from "axios";
import NavBar from '../../components/NavBar';

const Users = () => {
  let [users, setUsers] = useState([]);
  let [error, setError] = useState(null);
  const [api, contextHolder] = notification.useNotification();
  const contextValue = useMemo(
    () => ({
      name: 'Ant Design',
    }),
    [],
  );

  const fetchUsers = async () => {
    let { data, status} = await axios.get('http://127.0.0.1:4000/api/users/');
    setUsers(data);
  };

  const updateUser = (user) => {
    // raise modal and form edit movie
  };
  const openNotification = (message, placement) => {
    api.success({
      message: `Notification ${placement}`,
      description: message,
      placement,
    });
  };

  const deleteUser = async (id) => {
    // delete movie
    let { data, status} = await axios.delete(`http://127.0.0.1:4000/api/users/delete/${id}`);
    if (status === 200) {
      let newUsers = users.filter(user => user.id != id);
      openNotification(data['message'], 'Delete Resource');
      setMovies(newUsers);
    }
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  return(
    <>
    {contextHolder}
    <NavBar/>
      <Row justify="center">
        <Col span={12}>
          { users ?
            <Table columns={[
              { title: 'Name', dataIndex: 'first_name', key: 'name'},
              { title: 'Email', dataIndex: 'email', key: 'email'},
              { title: 'Last Name', dataIndex: 'last_name', key: 'stars'},
              { title: 'ID', dataIndex: 'id', key: 'id' },
              // { title: 'Actions', key: 'actions', render: (_, record) => (
              //   <Space>
              //     <Link href={`/users/${record.id}`}>
              //       <Button>View</Button>
              //     </Link>
              //     <Button onClick={() => updateUser(record)}>Update</Button>
              //     <Button onClick={() => deleteUser(record.id)}>Delete</Button>
              //   </Space>
              // )}
            ]} dataSource={users}
            pagination={false} /> : null
        }
        </Col>
      </Row>
    </>
  )
};

export default Users;